/*    */ package de.robin-stan.main;
/*    */ 
/*    */ import de.robin-stan.utils.LogLevel;
/*    */ import de.robin-stan.utils.Logger;
/*    */ import de.robin-stan.utils.MessageFileManager;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.plugin.Plugin;
/*    */ import org.bukkit.plugin.java.JavaPlugin;
/*    */ 
/*    */ public class Main
/*    */   extends JavaPlugin {
/* 13 */   private static String defaultFilePath = "plugins//AutoBroadcast";
/*    */   
/*    */   public static int taskID;
/*    */   
/* 17 */   private static final Logger logger = new Logger();
/*    */   
/* 19 */   private static int messageID = 0;
/*    */   
/*    */   private static boolean messageExits;
/*    */   
/*    */   public void onEnable() {
/* 24 */     loadFiles();
/* 25 */     startBroadcasting();
/*    */   }
/*    */ 
/*    */   
/*    */   public void onDisable() {
/* 30 */     Bukkit.getScheduler().cancelTask(taskID);
/*    */   }
/*    */   
/*    */   private void loadFiles() {
/* 34 */     MessageFileManager messageFileManager = new MessageFileManager();
/* 35 */     messageFileManager.loadFile();
/* 36 */     messageExits = messageFileManager.loadMessages();
/* 37 */     messageFileManager.loadSettings();
/*    */   }
/*    */ 
/*    */   
/*    */   public static void startBroadcasting() {
/* 42 */     taskID = Bukkit.getScheduler().scheduleAsyncRepeatingTask((Plugin)getPlugin(Main.class), new Runnable() {
/*    */           public void run() {
/* 44 */             if (!Main.messageExits) {
/* 45 */               Main.logger.log("Keine Nachrichten vorhanden.", LogLevel.INFO);
/* 46 */               Bukkit.getScheduler().cancelTask(Main.taskID);
/* 47 */               Main.logger.log("Es ist ein Fehler aufgetreten. Plugin wird gestoppt. Bitte Robin kontaktiern!", LogLevel.INFO);
/* 48 */               Bukkit.getPluginManager().disablePlugin((Plugin)Main.getPlugin(Main.class));
/*    */               return;
/*    */             } 
/* 51 */             if (MessageFileManager.messages.get(Integer.valueOf(Main.messageID)) == null)
/* 52 */               Main.messageID = 0; 
/* 53 */             String currentMessage = (String)MessageFileManager.messages.get(Integer.valueOf(Main.messageID));
/* 54 */             currentMessage = currentMessage.replace("%prefix%", MessageFileManager.getPrefix());
/* 55 */             currentMessage = currentMessage.replace("%onlineSize%", String.valueOf(Bukkit.getOnlinePlayers().size()));
/* 56 */             currentMessage = currentMessage.replace("&", "§");
/* 57 */             if (MessageFileManager.isEmtpyLines()) {
/* 58 */               for (Player online : Bukkit.getOnlinePlayers()) {
/* 59 */                 online.sendMessage("");
/* 60 */                 online.sendMessage(currentMessage);
/* 61 */                 online.sendMessage("");
/*    */               } 
/*    */             } else {
/* 64 */               for (Player online : Bukkit.getOnlinePlayers())
/* 65 */                 online.sendMessage(currentMessage); 
/*    */             } 
/* 67 */             Main.messageID = Main.messageID + 1;
/*    */           }
/* 69 */         }20L, (20 * MessageFileManager.getDelayInSeconds().intValue()));
/*    */   }
/*    */   
/*    */   public static String getDefaultFilePath() {
/* 73 */     return defaultFilePath;
/*    */   }
/*    */   
/*    */   public static void setDefaultFilePath(String defaultFilePath) {
/* 77 */     Main.defaultFilePath = defaultFilePath;
/*    */   }
/*    */ }


/* Location:              C:\Users\Maze\Downloads\AutoBroadcast_1.jar!\de\mailserver\main\Main.class
 * Java compiler version: 8 (52.0)
 * Author Robin S.Kaulfu?
 * JD-Core Version:       1.1.3
 */