/*    */ package de.robin-stan.utils;
/*    */ 
/*    */ import de.robin-stan.main.Main;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.command.Command;
/*    */ import org.bukkit.command.CommandExecutor;
/*    */ import org.bukkit.command.CommandSender;
/*    */ import org.bukkit.plugin.PluginDescriptionFile;
/*    */ 
/*    */ 
/*    */ public class AutoMessageCommand
/*    */   implements CommandExecutor
/*    */ {
/*    */   public AutoMessageCommand(String command) {
/* 15 */     Bukkit.getPluginCommand(command).setExecutor(this);
/*    */   }
/*    */   
/*    */   public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
/* 19 */     String prefix = MessageFileManager.getPrefix().replace("&", "§");
/* 20 */     if (sender.hasPermission("automessage.admin")) {
/* 21 */       if (args.length == 0) {
/* 22 */         PluginDescriptionFile pluginDescriptionFile = ((Main)Main.getPlugin(Main.class)).getDescription();
/* 23 */         sender.sendMessage(String.valueOf(prefix) + "§7Automessage v" + pluginDescriptionFile.getVersion());
/* 24 */         sender.sendMessage(String.valueOf(prefix) + "§7By§8: §a" + pluginDescriptionFile.getAuthors().toString().replace("[", "") + pluginDescriptionFile.getAuthors().toString().replace("]", ""));
/* 25 */         sender.sendMessage(String.valueOf(prefix) + " §a /automessage reload §7- Lädt die Config neu.");
/* 26 */         sender.sendMessage(String.valueOf(prefix) + " §a /automessage stop §7- stop den Broadcaster.");
/* 27 */         sender.sendMessage(String.valueOf(prefix) + " §a /automessage start §7- startet den Messager neu.");
/* 28 */       } else if (args.length == 1) {
/* 29 */         if (args[0].equalsIgnoreCase("reload")) {
/* 30 */           (new MessageFileManager()).loadSettings();
/* 31 */           (new MessageFileManager()).loadMessages();
/* 32 */           sender.sendMessage(String.valueOf(MessageFileManager.getPrefix().replace("&", "§")) + "§7 Plugin erfolgreich neu Geladen!");
/* 33 */         } else if (args[0].equalsIgnoreCase("stop")) {
/* 34 */           if (Bukkit.getScheduler().isCurrentlyRunning(Main.taskID)) {
/* 35 */             Bukkit.getScheduler().cancelTask(Main.taskID);
/* 36 */             sender.sendMessage(String.valueOf(prefix) + " §7Broadcaster wurde gestoppt!");
/*    */           } else {
/* 38 */             sender.sendMessage(String.valueOf(prefix) + " §7Es wird keine Nachricht verbreitet!");
/*    */           } 
/* 40 */         } else if (args[0].equalsIgnoreCase("start")) {
/* 41 */           if (!Bukkit.getScheduler().isCurrentlyRunning(Main.taskID)) {
/* 42 */             Main.startBroadcasting();
/* 43 */             sender.sendMessage(String.valueOf(prefix) + " §7Broadcaster gestartet");
/*    */           } else {
/* 45 */             sender.sendMessage(String.valueOf(prefix) + " §7Broadcaster läuft bereits");
/*    */           } 
/*    */         } else {
/* 48 */           sender.sendMessage(String.valueOf(prefix) + " §7Der Befehl existiert nicht!");
/*    */         } 
/*    */       } 
/*    */     } else {
/* 52 */       sender.sendMessage(MessageFileManager.getNoPermission().replace("%prefix%", prefix).replace("&", "§"));
/*    */     } 
/* 54 */     return false;
/*    */   }
/*    */ }


/* Location:              C:\Users\Robin\Documents\Automessages.jar!\de\robin-stan\\utils\AutoMessageCommand.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */