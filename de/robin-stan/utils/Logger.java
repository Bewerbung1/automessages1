/*    */ package de.robin-stan.utils;
/*    */ 
/*    */ import org.bukkit.Bukkit;
/*    */ 
/*    */ public class Logger
/*    */ {
/*    */   public void log(LogLevel logLevel, Object message) {
/*  8 */     switch (logLevel) {
/*    */       case INFO:
/* 10 */         Bukkit.getConsoleSender().sendMessage("§7[Info]:" + message);
/*    */         break;
/*    */       case WARN:
/* 13 */         Bukkit.getConsoleSender().sendMessage("§7[Warn]:" + message);
/*    */         break;
/*    */       case null:
/* 16 */         Bukkit.getConsoleSender().sendMessage("§7[Error]:" + message);
/*    */         break;
/*    */     } 
/*    */   }
/*    */   
/*    */   public void log(Object message, LogLevel logLevel) {
/* 22 */     switch (logLevel) {
/*    */       case INFO:
/* 24 */         Bukkit.getConsoleSender().sendMessage("§7[Info]:" + message);
/*    */         break;
/*    */       case WARN:
/* 27 */         Bukkit.getConsoleSender().sendMessage("§7[Warn]:" + message);
/*    */         break;
/*    */       case null:
/* 30 */         Bukkit.getConsoleSender().sendMessage("§7[Error]:" + message);
/*    */         break;
/*    */     } 
/*    */   }
/*    */ }


/* Location:              C:\Users\Robin\Documents\Automessages.jar!\de\robin-stan\\utils\Logger.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */