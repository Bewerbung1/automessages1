/*     */ package de.robin-stan.utils;
/*     */ 
/*     */ import de.robin-stan.main.Main;
/*     */ import java.io.File;
/*     */ import java.io.IOException;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import org.bukkit.configuration.file.YamlConfiguration;
/*     */ 
/*     */ public class MessageFileManager
/*     */ {
/*  13 */   public static HashMap<Integer, String> messages = new HashMap<>();
/*     */   
/*  15 */   private final Logger logger = new Logger();
/*     */   
/*     */   private static boolean emptyLines;
/*     */   
/*     */   private static String prefix;
/*     */   
/*     */   private static Integer delayInSeconds;
/*     */   
/*     */   private static String noPermission;
/*     */   
/*     */   public void loadFile() {
/*  26 */     File file = new File(Main.getDefaultFilePath(), "messages.yml");
/*  27 */     if (!file.exists()) {
/*  28 */       File folder = new File(Main.getDefaultFilePath());
/*  29 */       if (!folder.exists())
/*  30 */         folder.mkdirs(); 
/*     */       try {
/*  32 */         file.createNewFile();
/*  33 */         writeDefault(file);
/*  34 */       } catch (IOException e) {
/*  35 */         e.printStackTrace();
/*     */       } 
/*     */     } 
/*     */   }
/*     */   
/*     */   public boolean loadMessages() {
/*  41 */     File file = new File(Main.getDefaultFilePath(), "messages.yml");
/*  42 */     YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
/*  43 */     List<String> sourceList = configuration.getStringList("Messages");
/*  44 */     if (sourceList.isEmpty()) {
/*  45 */       this.logger.log("Bitte lege eine Message in der messages.yml fest!", LogLevel.WARN);
/*  46 */       return false;
/*     */     } 
/*  48 */     int iterationID = 0;
/*  49 */     Iterator<String> iterator = sourceList.iterator();
/*  50 */     while (iterator.hasNext()) {
/*  51 */       if (iterator.next() == null)
/*  52 */         this.logger.log("Alle Nachrichten neu Geladen!", LogLevel.INFO); 
/*  53 */       messages.put(Integer.valueOf(iterationID), sourceList.get(iterationID));
/*  54 */       iterationID++;
/*     */     } 
/*  56 */     return true;
/*     */   }
/*     */   
/*     */   public void loadSettings() {
/*  60 */     File file = new File(Main.getDefaultFilePath(), "messages.yml");
/*  61 */     YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
/*  62 */     setEmtpyLines(configuration.getBoolean("Settings.EmptyLines"));
/*  63 */     setDelayInSeconds(Integer.valueOf(configuration.getInt("Settings.DelayInSeconds")));
/*  64 */     setPrefix(configuration.getString("Settings.Prefix"));
/*  65 */     setNoPermission(configuration.getString("Notify.NoPermission"));
/*     */   }
/*     */   
/*     */   private void writeDefault(File file) {
/*  69 */     YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
/*  70 */     List<String> messages = configuration.getStringList("Messages");
/*  71 */     messages.add("%prefix% &7Schaue auf unserem TS vorbei: &bts.trysurvive.de&8.");
/*  72 */     messages.add("%prefix% &7Derzeit sind &a%onlineSize% &7Spiele(r) Online auf CityBuild!");
/*  73 */     configuration.set("Messages", messages);
/*  74 */     configuration.set("Settings.DelayInSeconds", Integer.valueOf(10));
/*  75 */     configuration.set("Settings.Prefix", "&8[&3SERVER&8]");
/*  76 */     configuration.set("Settings.EmptyLines", Boolean.valueOf(true));
/*  77 */     configuration.set("Notify.NoPermission", "%prefix% &cDu hast dafür keine Berechtigung.");
/*     */     try {
/*  79 */       configuration.save(file);
/*  80 */     } catch (IOException e) {
/*  81 */       e.printStackTrace();
/*     */     } 
/*     */   }
/*     */   
/*     */   public static String getNoPermission() {
/*  86 */     return noPermission;
/*     */   }
/*     */   
/*     */   public static void setNoPermission(String noPermission) {
/*  90 */     MessageFileManager.noPermission = noPermission;
/*     */   }
/*     */   
/*     */   public static Integer getDelayInSeconds() {
/*  94 */     return delayInSeconds;
/*     */   }
/*     */   
/*     */   public static void setDelayInSeconds(Integer delayInSeconds) {
/*  98 */     MessageFileManager.delayInSeconds = delayInSeconds;
/*     */   }
/*     */   
/*     */   public static String getPrefix() {
/* 102 */     return prefix;
/*     */   }
/*     */   
/*     */   public static void setPrefix(String prefix) {
/* 106 */     MessageFileManager.prefix = prefix;
/*     */   }
/*     */   
/*     */   public static boolean isEmtpyLines() {
/* 110 */     return emptyLines;
/*     */   }
/*     */   
/*     */   public static void setEmtpyLines(boolean emtpyLines) {
/* 114 */     emptyLines = emtpyLines;
/*     */   }
/*     */ }


/* Location:              C:\Users\Robin\Documents\Automessages.jar!\de\robin-stan\\utils\MessageFileManager.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */